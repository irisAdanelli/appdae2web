﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AppDAE2Web.Areas.Asistencia.Services;
using AppDAE2Web.Models;


namespace AppDAE2Web.Areas.Asistencia.Controllers
{
    public class CatEdificiosController : Controller
    {
        public IActionResult FicViCatEdificiosLista()
        {
            try
            {
                FicSrvCatEdificiosLista FicServicio = new FicSrvCatEdificiosLista();

                List<eva_cat_edificios> FicLista = FicServicio.FicGetCatEdificios().Result;
                ViewBag.Title = "Catalogo";

                return View(FicLista);

            }
            catch (Exception)
            {
                throw;
            }
        }


        public IActionResult FicViCatEdificiosDetail(eva_cat_edificios item)
        {
            try
            {
                return View(item);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult FicViCatEdificiosNew(eva_cat_edificios item) {
            try
            {
                return View(item);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult FicPostItemCatEdficios(eva_cat_edificios FicPaCatEdificios)
        {
            try
            {
                //FIC: Aqui se crea la instancia del servicio que tendra los metodos relacionados con la vista
                FicSrvCatEdificiosLista FicServicio = new FicSrvCatEdificiosLista();

                //Fic: Aqui 
                String r = FicServicio.FicCatEdificiosEdit(FicPaCatEdificios).Result;
                ViewBag.Title = "Catalogo de edificios";

                return RedirectToAction("FicViCatEdificiosLista");
            }
            catch (Exception)
            {
                throw;
            }
        }


        public IActionResult FicViCatEdificiosEdit(eva_cat_edificios FicPaCatEdificios)
        {
            return View(FicPaCatEdificios);
        }
        public IActionResult FicPutItemCatEdficios(eva_cat_edificios FicPaCatEdificios)
        {
            try
            {
                //FIC: Aqui se crea la instancia del servicio que tendra los metodos relacionados con la vista
                FicSrvCatEdificiosLista FicServicio = new FicSrvCatEdificiosLista();

                //Fic: Aqui 
                String r = FicServicio.FicCatEdificiosEdit(FicPaCatEdificios).Result;
                ViewBag.Title = "Catalogo de edificios";

                return RedirectToAction("FicViCatEdificiosLista");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}