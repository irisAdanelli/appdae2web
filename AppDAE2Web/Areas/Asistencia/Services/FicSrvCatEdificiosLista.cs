﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using AppDAE2Web.Models;
using System.Text;

namespace AppDAE2Web.Areas.Asistencia.Services
{
    public class FicSrvCatEdificiosLista
    {
        HttpClient ficCliente = new HttpClient();

        public FicSrvCatEdificiosLista()
        {
            ficCliente.BaseAddress = new Uri("http://localhost:53380");
            // http://localhost:53380/api/FicGetListCatEdificios 
            ficCliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<eva_cat_edificios>> FicGetCatEdificios()
        {
            try
            {
                HttpResponseMessage ficRespons = await ficCliente.GetAsync("api/FicGetListCatEdificios");
                if (ficRespons.IsSuccessStatusCode)
                {
                    var ficRespuesta = await ficRespons.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<eva_cat_edificios>>(ficRespuesta);

                }
                return new List<eva_cat_edificios>();
            }
            catch (Exception)
            {
                throw;
            }

        }

        //FicSrvCatEdificiosEdit
        public async Task<String>FicCatEdificiosEdit(eva_cat_edificios FicPaCatEdificios)
         {
             try
             {
                HttpResponseMessage FicResponse = await ficCliente.PutAsync($"api/FicPutItemCatEdificio/{FicPaCatEdificios.IdEdificio}",
                                                      new StringContent(JsonConvert.SerializeObject(FicPaCatEdificios),
                                                                          Encoding.UTF8, "application/json"));
                return "TODO SALIO BIEN";
            
             }
             catch (Exception)
             {
                 throw;
             }

         }
    }
}
