﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace AppDAE2Web.Models
{
    public class cat_estatus
    {

        [ForeignKey("cat_tipo_estatus"), Required]
        public Int16? IdTipoEstatus { get; set; }

        [Key, Required]
        public Int16? IdEstatus { get; set; }

        public String Clave { get; set; }

        public String DesEstatus { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Activo { get; set; }

        public DateTime? FechaReg { get; set; }

        public DateTime? FechaUltMod { get; set; }

        public String UsuarioReg { get; set; }

        public String UsuarioMod { get; set; }

        [StringLength(1, ErrorMessage = "Borrado no puede ser más de 1 caracter")]
        public bool Borrado { get; set; }
    }

    public class cat_tipo_estatus
    {
        [Key, Required]
        public Int16? IdTipoEstatus { get; set; }

        public String DesTipoEstatus { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Activo { get; set; }

        public DateTime? FechaReg { get; set; }

        public String UsuarioReg { get; set; }

        public DateTime? FechaUltMod { get; set; }

        public String UsuarioMod { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Borrado { get; set; }
    }

    public class eva_cat_edificios
    {
        [Key, Required]
        public Int16? IdEdificio { get; set; }
        public String Alias { get; set; }
        public String DesEdificio { get; set; }
        public Int16? Prioridad { get; set; }
        public String Clave { get; set; }
        public DateTime? FechaReg { get; set; }
        public DateTime? FechaUltMod { get; set; }
        public String UsuarioReg { get; set; }
        public String UsuarioMod { get; set; }
        public bool Activo { get; set; }
        public bool Borrado { get; set; }
        public List<eva_cat_espacios> IdEspacio { get; set; }
    }

    public class eva_cat_espacios
    {
        //public int16 idedificio {get, set}
        [ForeignKey("eva_cat_edificios"), Required]
        public Int16? IdEdificio { get; set; }

        [Key, Required]
        public Int16? IdEspacio { get; set; }

        public String Clave { get; set; }

        public String DesEspacio { get; set; }

        public Int16? Prioridad { get; set; }

        [StringLength(10, ErrorMessage = "Alias no puede ser más de 10 caracteres")]
        public String Alias { get; set; }

        public Int16? RangoTiempoReserva { get; set; }

        public Int16? Capacidad;

        [ForeignKey("cat_tipo_estatus")]
        public Int16? IdTipoEstatus { get; set; }

        [ForeignKey("cat_estatus")]
        public Int16? IdEstatus { get; set; }

        public String RefeUbicacion { get; set; }

        [StringLength(1, ErrorMessage = "PermiteCruce no puede ser más de 1 caracter")]
        public String PermiteCruce { get; set; }

        public String Observacion { get; set; }

        public DateTime? FechaReg { get; set; }

        public DateTime? FechaUltMod { get; set; }

        public String UsuarioReg { get; set; }

        public String UsuarioMod { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Activo { get; set; }

        [StringLength(1, ErrorMessage = "Borrado no puede ser más de 1 caracter")]
        public bool Borrado { get; set; }
    }

    public class eva_cat_tipo_competencia
    {

        //public int16 IdTipoCompetencia {get, set}
        [Key, Required]
        public Int16? IdTipoCompetencia { get; set; }

        public String DesTipoCompetencia { get; set; }

        public String Detalle { get; set; }

        public DateTime? FechaReg { get; set; }

        public String UsuarioReg { get; set; }

        public DateTime? FechaUltMod { get; set; }

        public String UsuarioMod { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Activo { get; set; }

        [StringLength(1, ErrorMessage = "Borrado no puede ser más de 1 caracter")]
        public bool Borrado { get; set; }
    }

    public class eva_cat_competencia
    {

        //public int16 IdTipoCompetencia {get, set}
        [ForeignKey("eva_cat_competencia"), Required]
        public Int16? IdTipoCompetencia { get; set; }

        [Key, Required]
        public Int32? IdCompetencia { get; set; }

        public String DesTipoCompetencia { get; set; }

        public String Detalle { get; set; }

        public DateTime? FechaReg { get; set; }

        public String UsuarioReg { get; set; }

        public DateTime? FechaUltMod { get; set; }

        public String UsuarioMod { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Activo { get; set; }

        [StringLength(1, ErrorMessage = "Borrado no puede ser más de 1 caracter")]
        public bool Borrado { get; set; }
    }

    public class eva_cat_conocimiento
    {
        /*
        * IdCompetencia int NOT NULL,
	    * -- DesConocimiento varchar(255) NULL, 
	    * Detalle varchar(3000) NULL,
	    * FechaReg datetime NULL, 
	    * UsuarioMod varchar(20) NULL, 
	    * -- FechaUhMod datetime NULL, 
	    * UsuarioReg varchar(20) NULL,
	    * Activo char(1) NULL, 
	    * Borrado char(1) NULL, 
        */

        //public int16 IdCompetencia {get, set}
        [ForeignKey("eva_cat_competencia"), Required]
        public Int32? IdCompetencia { get; set; }

        [Key, Required]
        public Int16? IdConocimiento { get; set; }

        public String DesConocimiento { get; set; }

        public String Detalle { get; set; }

        public DateTime? FechaReg { get; set; }

        public String UsuarioReg { get; set; }

        public DateTime? FechaUhMod { get; set; }

        public String UsuarioMod { get; set; }

        [StringLength(1, ErrorMessage = "Activo no puede ser más de 1 caracter")]
        public bool Activo { get; set; }

        [StringLength(1, ErrorMessage = "Borrado no puede ser más de 1 caracter")]
        public bool Borrado { get; set; }
    }
}
